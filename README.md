# Spring Batch: Step Listener with ItemReadListener

In this project you will learn how to intercept the flow 
of reading steps with ItemReadListener using Spring Batch architecture.