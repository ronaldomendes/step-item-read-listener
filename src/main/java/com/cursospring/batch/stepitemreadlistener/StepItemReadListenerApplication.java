package com.cursospring.batch.stepitemreadlistener;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.stepitemreadlistener"})
public class StepItemReadListenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StepItemReadListenerApplication.class, args);
    }

}
