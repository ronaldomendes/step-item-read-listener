package com.cursospring.batch.stepitemreadlistener.listener;

import com.cursospring.batch.stepitemreadlistener.dto.EmployeeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemReadListener;

@Slf4j
public class ReaderListener implements ItemReadListener<EmployeeDTO> {

    @Override
    public void beforeRead() {
        log.info("Before read operation");
    }

    @Override
    public void afterRead(EmployeeDTO dto) {
        log.info("After read operation: {}", dto.toString());
    }

    @Override
    public void onReadError(Exception ex) {
        log.error("On read error: {}", ex.getMessage());
    }
}
